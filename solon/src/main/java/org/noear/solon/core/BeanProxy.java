package org.noear.solon.core;

/**
 * Bean 代理接口
 * */
public interface BeanProxy {
    Object getProxy(Object bean);
}
